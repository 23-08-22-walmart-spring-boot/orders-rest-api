package com.classpath.orders.service;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.classpath.orders.model.Order;
import com.classpath.orders.model.OrderDto;
import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {
	
	private final OrderRepository orderRepository;
	
	public Order saveOrder(Order order) {
		return this.orderRepository.save(order);
	}
	
	public Map<String, Object> fetchAllOrders(int page, int size, String sortDirection, String property){
		
		Sort.Direction direction = sortDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC: Sort.Direction.DESC;
		PageRequest pageRequest = PageRequest.of(page, size, direction, property);
		Page<OrderDto> pageResponse = this.orderRepository.findBy(pageRequest);
		
		long totalRecords = pageResponse.getTotalElements();
		int records = pageResponse.getSize();
		int totalPages = pageResponse.getTotalPages();
		List<OrderDto> data = pageResponse.getContent();
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("total-records", totalRecords);
		response.put("pages", totalPages);
		response.put("size", records);
		response.put("data", data);
		return response;
	}
	
	public Order findOrderById(long orderId) {
		return this.orderRepository
					.findById(orderId)
					.orElseThrow(() -> new IllegalArgumentException("invalid order id"));
	}
	
	public Order updateOrderById(long orderId, Order updatedOrder) {

		Order order = this.findOrderById(orderId);
		order.setCustomerName(updatedOrder.getCustomerName());
		order.setLineItems(updatedOrder.getLineItems());
		order.setOrderDate(updatedOrder.getOrderDate());
		order.setOrderPrice(updatedOrder.getOrderPrice());
		return this.orderRepository.save(order);
		
	}
	
	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
	}
	
public Map<String, Object> fetchAllOrdersByPriceRange(int page, int size, double min, double max){
		
		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "customerName");
		Page<Order> pageResponse = this.orderRepository.findByOrderPriceBetween(min, max, pageRequest);
		
		long totalRecords = pageResponse.getTotalElements();
		int records = pageResponse.getSize();
		int totalPages = pageResponse.getTotalPages();
		List<Order> data = pageResponse.getContent();
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("total-records", totalRecords);
		response.put("pages", totalPages);
		response.put("size", records);
		response.put("data", data);
		return response;
	}  

}
