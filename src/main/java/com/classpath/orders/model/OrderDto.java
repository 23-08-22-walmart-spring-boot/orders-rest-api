package com.classpath.orders.model;

import java.time.LocalDate;

public interface OrderDto {
	
	String getCustomerName();
	
	double getOrderPrice();
	
	LocalDate getOrderDate();
	
}
