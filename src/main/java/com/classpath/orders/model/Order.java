package com.classpath.orders.model;

import static lombok.AccessLevel.PRIVATE;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor(access = PRIVATE)
@Builder
@AllArgsConstructor
@EqualsAndHashCode(exclude = "lineItems")
@ToString(exclude = "lineItems")

@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotEmpty(message = "customer name cannot be empty")
	private String customerName;

	@NotEmpty(message = "customer email cannot be empty")
	@Email(message = "customer email is not in correct format")
	private String email;

	@Min(value = 200, message = "min order price should be 200")
	@Max(value = 8000, message = "max order price should be 8000")
	private double orderPrice;

	@PastOrPresent(message = "order date cannot be in the future")
	private LocalDate orderDate;

	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	private Set<LineItem> lineItems;

	/*
	 * scaffolding code
	 */
	public void addLineItem(LineItem lineItem) {
		if (this.lineItems == null) {
			this.lineItems = new HashSet<>();
		}
		this.lineItems.add(lineItem);
		lineItem.setOrder(this);
	}

}
