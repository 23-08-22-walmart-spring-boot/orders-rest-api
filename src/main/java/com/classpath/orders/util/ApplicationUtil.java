package com.classpath.orders.util;

import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile({"dev", "qa"})
public class ApplicationUtil implements CommandLineRunner {

	
	private final ApplicationContext applicationContext;
	
	public ApplicationUtil(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Inside the applicationUtil ::::");
		String[] beanNames = this.applicationContext.getBeanDefinitionNames();
		Stream.of(beanNames)
			.filter(beanName -> beanName.startsWith("user"))
			.forEach(beanName -> System.out.println(beanName));
	}

}
