package com.classpath.orders.util;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.classpath.orders.model.LineItem;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Configuration
@RequiredArgsConstructor
@Slf4j
@Profile("dev")
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent>{

	private final OrderRepository orderRepository;
	private final Faker faker = new Faker();
	
	@Value("${app.orderCount}")
	private int orderCount;
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		log.info("----------Application is ready - inserting records------------");
		IntStream.range(0, orderCount).forEach(index -> {
			String firstName = faker.name().firstName();
			Order order = Order.builder()
								.customerName(firstName)
								.orderDate(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
								.email(firstName+"@"+faker.internet().domainName())
								.build();
			IntStream.range(1, faker.number().numberBetween(2, 4))
					.forEach(value -> {
						LineItem lineItem = LineItem.builder()
								.name(faker.commerce().productName())
								.qty(faker.number().numberBetween(2, 5))
								.price(faker.number().randomDouble(2, 400, 800))
								.build();
						
						order.addLineItem(lineItem);
					});
			
			//order would have all the lineItems 
			// set the total price and save the order to the db
			double totalOrderPrice = order
			    .getLineItems()
			    .stream()
			    .map(lineItem -> lineItem.getQty() * lineItem.getPrice())
			    .reduce(0d, Double::sum);
			order.setOrderPrice(totalOrderPrice);
			this.orderRepository.save(order);
		});
		log.info("----------Application is ready - inserted records------------");
	}
}
