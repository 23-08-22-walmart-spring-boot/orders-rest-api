package com.classpath.orders.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.orders.model.Order;
import com.classpath.orders.service.OrderService;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	private final OrderService orderService;
	
	@GetMapping
	@ApiResponse(responseCode = "200",description = "paginated orders")
	public Map<String, Object> fetchAllOrder(
			  @RequestParam(name="page", defaultValue = "0", required = false) int page,
			  @RequestParam(name="size", defaultValue = "10", required = false) int size,
			  @RequestParam(name="sort", defaultValue = "asc", required = false)String sort,
			  @RequestParam(name="name", defaultValue = "customerName", required = false) String name){
		return this.orderService.fetchAllOrders(page, size, sort, name);
	}

	@GetMapping("/price")
	public Map<String, Object> fetchAllOrderByPriceRange(
			  @RequestParam(name="page", defaultValue = "0", required = false) int page,
			  @RequestParam(name="size", defaultValue = "10", required = false) int size,
			  @RequestParam(name="min", required = true)double minPrice,
			  @RequestParam(name="max", required = true) double maxPrice){
		return this.orderService.fetchAllOrdersByPriceRange(page, size, minPrice, maxPrice);
	}
	
	@GetMapping("/{id}")
	public Order fetchOrderById(@PathVariable("id") long orderId){
		return this.orderService.findOrderById(orderId);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody @Valid Order order) {
		return this.orderService.saveOrder(order);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable("id") long orderId) {
		this.orderService.deleteOrderById(orderId);
	}

}
