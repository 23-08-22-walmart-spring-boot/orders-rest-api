package com.classpath.orders.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classpath.orders.model.Order;
import com.classpath.orders.model.OrderDto;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
	
	//custom methods
	Page<Order> findByOrderPriceBetween(double minPrice, double maxPrice, Pageable page);
	
	Page<Order> findByOrderDateBetween(LocalDate start, LocalDate endDate, Pageable page);
	
	Page<OrderDto> findBy(Pageable pageRequest);
	

}
