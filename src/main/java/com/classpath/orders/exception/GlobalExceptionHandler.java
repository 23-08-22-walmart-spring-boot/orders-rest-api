package com.classpath.orders.exception;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Error handleException(IllegalArgumentException exception) {
		log.error("Invalid order id {}", exception.getMessage());
		return new Error(100, exception.getMessage());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Set<String> handleInvalidOrder(MethodArgumentNotValidException exception) {
		log.error("Invalid order data ");
		return exception
					.getAllErrors()
					.stream()
					.map(ObjectError :: getDefaultMessage)
					.collect(Collectors.toSet());
	}

}

@AllArgsConstructor
@Getter
class Error {
	private int code;
	private String message;
}
